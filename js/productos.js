import { initializeApp } from
"https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
import { getFirestore, doc, getDoc, getDocs, collection } from
"https://www.gstatic.com/firebasejs/9.4.0/firebase-firestore.js";
import { getStorage, ref as refS, uploadBytes,getDownloadURL} from
"https://www.gstatic.com/firebasejs/9.13.0/firebase-storage.js";
import { getDatabase, onValue, ref, set, get, child, update, remove } from
"https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";

const firebaseConfig = {
apiKey: "AIzaSyC0tkfGUZzR58eq0ZiBugqx06GBGbLbtL0",
authDomain: "dbweb-821b2.firebaseapp.com",
databaseURL: "https://dbweb-821b2-default-rtdb.firebaseio.com",
projectId: "dbweb-821b2",
storageBucket: "dbweb-821b2.appspot.com",
messagingSenderId: "913138267336",
appId: "1:913138267336:web:86cbe868016f6fbd8f61fd",
measurementId: "G-G1CYKN4WM6"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase();
const storage = getStorage();

// Generar Productos
let productos = document.getElementById('contenido-productos');
window.addEventListener('DOMContentLoaded',mostrarProductos)

function mostrarProductos(){
    const dbRef = ref(db, "dbweb");

    onValue(dbRef,(snapshot) => {
        productos.innerHTML = "";
        snapshot.forEach((childSnapshot) => {
            const childKey = childSnapshot.key;
            const childData = childSnapshot.val();
        
        if(childData.estatus=="0"){
            productos.innerHTML +=
            "<div class="+"'producto'"+"> " +
            "<img src="+" "+childData.urlImagen+" "+"> " +
            "<p class='nombre'>"+childData.nombre +"</p>"+
            "<p class='descripcion' style='font-size: 1em;'> "+childData.descripcion +"</p>"+
            "<p class='precio'>$"+childData.precio +"</p>"+
            "</div>";
        }
        
        });
    },
    {
        onlyOnce: true,
    }
    );
}